package at.itkolleg.ase.tdd.kino;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Dieses Beispiel stammt aus https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
 */
public class App {
    public static void main(String[] args) {

        //Saal anlegen
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        KinoSaal ks = new KinoSaal("LadyX", map);
        KinoSaal ks2 = new KinoSaal("Cinema", map);

        //Platz prüfen
        System.out.println(ks.pruefePlatz('A', 11));
        System.out.println(ks.pruefePlatz('A', 10));
        System.out.println(ks.pruefePlatz('B', 10));
        System.out.println(ks.pruefePlatz('C', 14));

        //Vorstellungen anlegen
        Vorstellung vorstellung = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.of(2022, 4, 8), "Superfilm", 19.9F);
        Vorstellung vorstellung1 = new Vorstellung(ks, Zeitfenster.NACHMITTAG, LocalDate.of(2022, 5, 23), "Spiderman", 20.0F);
        Vorstellung vorstellung2 = new Vorstellung(ks2, Zeitfenster.NACHT, LocalDate.of(2022, 6, 20), "Batman", 22.5F);

        // Vorstellungen über die Kinoverwaltung einplanen
        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);
        kinoVerwaltung.einplanenVorstellung(vorstellung1);
        kinoVerwaltung.einplanenVorstellung(vorstellung2);

        // Tickets für Vorstellungen ausgeben
        kinoVerwaltung.kaufeTicket(vorstellung,'A', 10,50.0F);
        kinoVerwaltung.kaufeTicket(vorstellung,'B', 5,30.0F);
        kinoVerwaltung.kaufeTicket(vorstellung,'C', 3,44.0F);

    }
}
