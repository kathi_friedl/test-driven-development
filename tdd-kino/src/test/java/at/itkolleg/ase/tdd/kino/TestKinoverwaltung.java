package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.sql.Array;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

public class TestKinoverwaltung {


    KinoVerwaltung verwaltung;
    Vorstellung vorstellung;
    Vorstellung vorstellung1;
    Vorstellung vorstellung2;

    @BeforeEach
    public void setUp() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        KinoSaal ks = new KinoSaal("LadyX", map);
        vorstellung = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.of(2022, 4, 8), "Superfilm", 19.9F);
        Vorstellung vorstellung1 = new Vorstellung(ks, Zeitfenster.NACHMITTAG, LocalDate.of(2022, 5, 23), "Spiderman", 20.0F);
        Vorstellung vorstellung2 = new Vorstellung(ks, Zeitfenster.NACHT, LocalDate.of(2022, 6, 20), "Batman", 22.5F);
        verwaltung = new KinoVerwaltung();

    }


    @DisplayName("Eine Vorstellung einplanen")
    @Test
    public void einplanenVorstellung() {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.ABEND,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);
        verwaltung.einplanenVorstellung(vorstellung);

        Assertions.assertEquals(1,
                verwaltung.getVorstellungen().size(),
                "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben.");
        Assertions.assertEquals(vorstellung,
                verwaltung.getVorstellungen().get(0),
                "Die Vorstellung wurde nicht korrekt gespeichert.");
    }

    private KinoSaal buildKinoSaal(String name, char maxReihe, int sitzeInReihe) {
        Map<Character, Integer> plaetze = new HashMap<>();
        for (char a = 'A'; a <= maxReihe; a++) {
            plaetze.put(a, sitzeInReihe);
        }
        return new KinoSaal(name, plaetze);
    }


    @DisplayName("Mehrere unterschiedliche Vorstellungen einplanen.")
    @Test
    public void einplanenVorstellungMultiple() {
        Vorstellung vorstellung1 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.NACHMITTAG,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);

        Vorstellung vorstellung2 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.ABEND,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);

        Vorstellung vorstellung3 = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.NACHT,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);

        verwaltung.einplanenVorstellung(vorstellung1);
        verwaltung.einplanenVorstellung(vorstellung2);
        verwaltung.einplanenVorstellung(vorstellung3);

        Assertions.assertAll("Vorstellungen",
                () -> Assertions.assertEquals(3,
                        verwaltung.getVorstellungen().size(),
                        "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben."),
                () -> Assertions.assertEquals(vorstellung1,
                        verwaltung.getVorstellungen().get(0),
                        "Die erste Vorstellung wurde nicht korrekt gespeichert."),
                () -> Assertions.assertEquals(vorstellung2,
                        verwaltung.getVorstellungen().get(1),
                        "Die zweite Vorstellung wurde nicht korrekt gespeichert."),
                () -> Assertions.assertEquals(vorstellung3,
                        verwaltung.getVorstellungen().get(2),
                        "Die dritte Vorstellung wurde nicht korrekt gespeichert."));
    }


    @DisplayName("Fehler wenn Vorestellung doppelt eingeplant.")
    @Test
    public void einplanenVorstellungDoppelt() {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.ABEND,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            verwaltung.einplanenVorstellung(vorstellung);
            verwaltung.einplanenVorstellung(vorstellung);
        });

        Assertions.assertEquals(exception.getMessage(), "Die Vorstellung ist bereits eingeplant");
        Assertions.assertEquals(1,
                verwaltung.getVorstellungen().size(),
                "Es wurde die falsche Anzahl registrierter Vorstellungen angegeben.");
    }

    @DisplayName("KaufeTicket")
    @ParameterizedTest
    @CsvSource({"A, 1, 10", "B, 5, 9.5", "C, 10, 9.5", "E, 1, 20", "F, 10, 100", "A, 1, 25.25"})
    public void kaufeTicket(char reihe, int platz, float geld) {
        Vorstellung vorstellung = new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.ABEND,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f);
        verwaltung.einplanenVorstellung(vorstellung);

        Ticket ticket = verwaltung.kaufeTicket(vorstellung, reihe, platz, geld);

        Assertions.assertAll("Ticket",
                () -> Assertions.assertEquals("Scala", ticket.getSaal()),
                () -> Assertions.assertEquals(Zeitfenster.ABEND, ticket.getZeitfenster()),
                () -> Assertions.assertEquals(LocalDate.of(2018, 5, 12), ticket.getDatum()),
                () -> Assertions.assertEquals(reihe, ticket.getReihe()),
                () -> Assertions.assertEquals(platz, ticket.getPlatz()));
    }


    @DisplayName("Massentest Ticketkauf")
    @TestFactory
    public List<DynamicTest> kaufeTicketDDoS() {
        KinoVerwaltung verwaltung = new KinoVerwaltung();
        verwaltung.einplanenVorstellung(new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.NACHMITTAG,
                LocalDate.of(2018, 5, 12),
                "Hanni und Nanni",
                9.50f));
        verwaltung.einplanenVorstellung(new Vorstellung(buildKinoSaal("Scala", 'F', 10),
                Zeitfenster.ABEND,
                LocalDate.of(2018, 5, 12),
                "Iron Man 4",
                9.50f));


        List<DynamicTest> testListe = new ArrayList<>();
        Random r = new Random();

        for (int i = 0; i <= 100; i++) {
            int random = r.nextInt(1001);
            Vorstellung vorstellung = verwaltung.getVorstellungen().get(i % 2);
            char reihe = (char) ((random % 10) + 65);
            int platz = random % 15;
            int geld = random % 50;

            testListe.add(DynamicTest.dynamicTest(vorstellung.getFilm() + ", " + reihe + platz + ", " + geld + "€",
                            () -> Assertions.assertDoesNotThrow(() -> {
                                try {
                                    verwaltung.kaufeTicket(vorstellung,
                                            reihe,
                                            platz,
                                            geld);
                                } catch (IllegalArgumentException e) {
                                    boolean errGeld = "Nicht ausreichend Geld.".equals(e.getMessage());
                                    boolean errPlatz = e.getMessage()
                                            .contains("existiert nicht");
                                    Assertions.assertTrue(errGeld || errPlatz);
                                } catch (IllegalStateException e) {
                                    Assertions.assertTrue(e.getMessage()
                                            .contains("ist bereits belegt."));
                                }
                            })
                    )
            );
        }
        return testListe;
    }


    @Test
    public void testVorstellungEinplanen() {

        ArrayList<Throwable> errors = new ArrayList<>();
        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);
        kinoVerwaltung.einplanenVorstellung(vorstellung2);

        try {
            Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(vorstellung));
        } catch (AssertionError assertionError) {
            errors.add(assertionError);
        }
        try {
            Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(vorstellung1));
        } catch (AssertionError assertionError) {
            errors.add(assertionError);
        }

        try {
            Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(vorstellung1));
        } catch (AssertionError assertionError) {
            errors.add(assertionError);
        }

        try {
            Assertions.assertThrows(IllegalArgumentException.class, () -> kinoVerwaltung.einplanenVorstellung(vorstellung2));
        } catch (AssertionError assertionError) {
            errors.add(assertionError);
        }

        if (errors.size() == 2) {
            System.out.println(errors);
            Assertions.fail();
        }

    }


}
