package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.*;

public class TestKinoSaal {

    private KinoSaal kinoSaal;

    @BeforeEach
    public void setUp() {
        HashMap map  = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        map.put('D', 12);

        kinoSaal = new KinoSaal("Kinosaal1", map);
    }

    @DisplayName("Teste Namen holen")
    @Test
    public void testHoleName() {
        Assertions.assertEquals("Kinosaal1", kinoSaal.getName());
    }


    @Test
    public void testEquals(){
        Assertions.assertFalse(kinoSaal.equals("Test"));
        Assertions.assertTrue(kinoSaal.equals(kinoSaal));
    }


    @TestFactory
    Collection<DynamicTest> dynamicTestPruefePlatz() {

        List<DynamicTest> testListe = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            Random random = new Random();
            int platz = random.nextInt(20) + 1;
            System.out.println(platz);
            if (platz > 10) {
                testListe.add(
                        DynamicTest.dynamicTest(
                                "Falsch-Wert-Test",
                                () -> Assertions.assertFalse(kinoSaal.pruefePlatz('A', platz))));
            } else {
                testListe.add(
                        DynamicTest.dynamicTest(
                                "Korrekt-Wert-Test",
                                () -> Assertions.assertTrue(kinoSaal.pruefePlatz('A', platz))));
            }
        }

        return testListe;

    }


}
