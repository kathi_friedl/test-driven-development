package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class TestVorstellung {

    private Vorstellung vorstellung;


    @BeforeEach
    public void setUp() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        KinoSaal ks = new KinoSaal("LadyX", map);
        vorstellung = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.of(2022, 4, 8), "Superfilm", 19.9F);
    }

    @Test
    public void testKaufeTicketGueltig(){
        Ticket gekauft = vorstellung.kaufeTicket('A', 5, 20);
        Assertions.assertEquals('A', gekauft.getReihe());
        Assertions.assertEquals(5, gekauft.getPlatz());
        Assertions.assertEquals(vorstellung.getSaal().getName(), gekauft.getSaal());
        Assertions.assertEquals(vorstellung.getZeitfenster(), gekauft.getZeitfenster());
    }

    @Test
    public void testKaufeTicketZuWenigGeld() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('A', 4, 3));
    }

    @Test
    public void testKaufeTicketFalscherUngueltigerPlatz() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('A', 19, 3));
    }

    @Test
    public void testKaufeTicketBelegterPlatz() {
        vorstellung.kaufeTicket('A', 5, 20);
        Assertions.assertThrows(IllegalStateException.class, () -> vorstellung.kaufeTicket('A', 5, 20));
    }



}
