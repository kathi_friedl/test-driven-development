import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CourseTest {

    CourseService courseService = Mockito.mock(CourseService.class);

    Course course = new Course(courseService);

    @Test
    void testAverage(){
        Mockito.when(courseService.getTotalRegistrations()).thenReturn(500);
        Mockito.when(courseService.getTotalCourses()).thenReturn(10);
        Assertions.assertEquals(50, course.getAverageMarks());
    }
}
