public interface CourseService {

    int getTotalRegistrations();
    int getTotalCourses();
}
