public class Course {

    CourseService courseService;

    Course(CourseService courseService){
        this.courseService = courseService;
    }

    int getAverageMarks(){
        return courseService.getTotalRegistrations() / courseService.getTotalCourses();
    }
}
